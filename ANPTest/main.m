//
//  main.m
//  ANPTest
//
//  Created by nghia vo on 11/23/16.
//  Copyright © 2016 NghiaVo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
