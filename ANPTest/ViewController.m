//
//  ViewController.m
//  ANPTest
//
//  Created by nghia vo on 11/23/16.
//  Copyright © 2016 NghiaVo. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface ViewController () <FBSDKLoginButtonDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIButton *postFbButton;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.loginButton.delegate = self;
    self.loginButton.readPermissions = @[@"email", @"public_profile", @"user_friends"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // If user logged in -> show user info
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if (![user objectForKey:@"fbID"])
    {
        self.postFbButton.hidden = true;
    }
    else
    {
        NSString *email = [user objectForKey:@"email"];
        if (email.length > 0)
        {
            self.emailLabel.text = email;
        }
        
        self.usernameLabel.text = [user objectForKey:@"name"];
        
        [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[user objectForKey:@"avatarURL"]]
                                placeholderImage:[UIImage imageNamed:@"def_avatar"]];
        
        self.postFbButton.hidden = false;
    }
}

#pragma mark Facebook Login Delegate
- (void) loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
               error:(NSError *)error {
    if(!error) {
        // success
        if ([result.grantedPermissions containsObject:@"public_profile"]) {
            if ([FBSDKAccessToken currentAccessToken]) {
                
                __weak ViewController *weakSelf = self;
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.width(540).height(540), email, name, id, gender, birthday"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     if (!error)
                     {
                         NSLog(@"fetched user:%@", result);
                         
                         // Generate user info
                         NSString *fbID = [result valueForKey:@"id"];
                         NSString *email = [result valueForKey:@"email"];
                         NSString *name = [result valueForKey:@"name"];
                         NSArray *pictureArr = [result objectForKey:@"picture"];
                         NSArray *dataArr = [pictureArr valueForKey:@"data"];
                         NSString *profilePicture = [dataArr valueForKey:@"url"];
                         
                         // Save user data
                         NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                         [user setObject:fbID forKey:@"fbID"];
                         
                         // User may log in with phone number -> we don't have email here
                         if (email.length > 0)
                         {
                             [user setObject:email forKey:@"email"];
                         }
                         
                         [user setObject:name forKey:@"name"];
                         [user setObject:profilePicture forKey:@"avatarURL"];
                         
                         // Update UI
                         if (email.length != 0)
                         {
                             weakSelf.emailLabel.text = email;
                         }
                         
                         weakSelf.usernameLabel.text = name;
                         [weakSelf.avatarImageView sd_setImageWithURL:[NSURL URLWithString:profilePicture]
                                       placeholderImage:[UIImage imageNamed:@"def_avatar"]];
                         weakSelf.postFbButton.hidden = false;
                     }
                 }];
            }
        }
    }
    else {
        // failed
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    // Clear data
    self.emailLabel.text = @"";
    self.usernameLabel.text = @"";
    self.avatarImageView.image = nil;
    self.postFbButton.hidden = true;
    
    // Remove user data
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
}

#pragma mark Button Action
/*
 *  Share facebook action
 */
- (IBAction)clickedOnPostFbButton:(id)sender {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"https://www.youtube.com/watch?v=VJLTURn35jQ&list=PLmbGhJt6y0ONEglibXJtMpT5xeoYtmAa4"];
    content.contentTitle = @"NHẠC LÍNH ĐAN NGUYÊN, GIANG TỬ - Tuyệt phẩm nhạc vàng, nhạc lính VNCH hay nhất";
    content.imageURL = [NSURL URLWithString:@"http://static-img.nhac.vui.vn/imageupload/upload2015/2015-4/singer/2015-12-11/1449802354_dannguyen.jpg"];
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
}
@end
